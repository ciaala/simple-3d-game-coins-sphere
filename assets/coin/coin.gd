extends Area

signal coinCollected

func _ready():
	pass

func _physics_process(_delta):
	rotate_y(deg2rad(5))


func _on_coin_body_entered(body):
	if body.name == 'Steve':
		$AnimationPlayer.play("coin-bouncing")
		$Timer.start()

func _on_Timer_timeout():
	print('on timer timeout')
	emit_signal('coinCollected')
	queue_free()
