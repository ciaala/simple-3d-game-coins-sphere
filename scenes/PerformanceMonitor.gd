extends MarginContainer
const MEGABYTE = 1024*1024;

func getAndRound(ELEMENT):
	return str(round(Performance.get_monitor(ELEMENT)/MEGABYTE))
	
func _physics_process(_delta):
	$VBoxContainer/Stat1.text = "FPS: " + str(Performance.get_monitor(Performance.TIME_FPS))
	$VBoxContainer/Stat2.text = "Mem Video: " + getAndRound(Performance.RENDER_VIDEO_MEM_USED)
	$VBoxContainer/Stat3.text = "Mem Stati: " + getAndRound(Performance.MEMORY_STATIC)
	$VBoxContainer/Stat4.text = "Mem Dynam: " + getAndRound(Performance.MEMORY_DYNAMIC)
