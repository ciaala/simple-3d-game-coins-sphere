extends KinematicBody

const SPEED = int(8)
const ROTATION_DEGREE = float(PI/2)

var PlayerName = String('Steve');
var velocity = Vector3(0,0,0);

func _ready():
	print('Hello, ' + PlayerName + ' !')

func updateVelocity(input1, input2, current, intesity):
	if Input.is_action_pressed(input1) and Input.is_action_pressed(input2):
		current = 0
	elif Input.is_action_pressed(input1):
		current = intesity
	elif Input.is_action_pressed(input2):
		current = -intesity
	else:
		current = lerp(current, 0, 0.05)
	return current
	
func _physics_process(_delta):
	velocity.x = updateVelocity("ui_left", "ui_right", velocity.x, SPEED)
	$MeshInstance.rotate_z(deg2rad(ROTATION_DEGREE * -velocity.x ))
	
	velocity.z = updateVelocity("ui_up", "ui_down", velocity.z, SPEED)
	$MeshInstance.rotate_x(deg2rad(ROTATION_DEGREE * velocity.z ))
	move_and_slide(velocity);


func _on_enemy_body_entered(body):
	if body.name == 'Steve': 
		get_tree().change_scene("res://scenes/SceneGameOver.tscn")
	else:
		print('enemy has touched something else: ' + body.name)
	pass # Replace with function body.
